import React from 'react'
import {connect} from 'react-redux'
import {Field, reduxForm} from 'redux-form'
import {Actions} from 'react-native-router-flux'
import {Image} from 'react-native'
import {
    View, Text, Content, Button, Container, Item, Form, Input, Icon,
} from 'native-base'
import Spinner from 'react-native-spinkit'
import {register, setRegisterErrors} from '../../../actions'
import {appStore} from '../../../'

class RegisterForm extends React.Component {

    componentDidMount() {
        this.props.reset()
        appStore.dispatch(setRegisterErrors(null))
    }
    renderField = ({input}) => {
        return <Input secureTextEntry={input.name === 'password' || input.name === 'confirmPassword'}
            keyboardType={input.name === 'email' ? 'email-address' : 'default'}
            style={{color: '#90a4ae', backgroundColor: '#011219'}} {...input } onChangeText={() => {
                if (this.props.errors) {
                    let errors = {}
                    for (const errorKey in this.props.errors) {
                        if (errorKey !== input.name) {
                            errors = {...errors, [errorKey]: this.props.errors[errorKey]}
                        }
                    }
                    appStore.dispatch(setRegisterErrors(errors))
                }
            }} placeholder={`${input.name}`} />
    }
    // eslint-disable-next-line complexity
    render() {
        return (
            <View>
                <Form>
                    <Item error={this.props.errors && this.props.errors.email ? true : false} regular style={{marginTop: 20}} >
                        <Field name="email" component={this.renderField} />
                        {this.props.errors && this.props.errors.email ? <Icon name="close-circle" /> : null}
                    </Item>
                    {this.props.errors && this.props.errors.email ? this.props.errors.email.map((error, index) => <Text note style={{color: 'red'}} key={index}>{error}</Text>) : null}
                    <Text></Text>
                    <Text></Text>
                    <Item error={this.props.errors && this.props.errors.password ? true : false} regular>
                        <Field name="password" component={this.renderField} />
                        {this.props.errors && this.props.errors.password ? <Icon name="close-circle" /> : null}
                    </Item>
                    {this.props.errors && this.props.errors.password ? this.props.errors.password.map((error, index) => <Text note style={{color: 'red'}} key={index}>{error}</Text>) : null}
                    <Text></Text>
                    <Text></Text>
                    <Item error={this.props.errors && this.props.errors.confirmPassword ? true : false} regular >
                        <Field name="confirmPassword" component={this.renderField} />
                        {this.props.errors && this.props.errors.confirmPassword ? <Icon name="close-circle" /> : null}
                    </Item>
                    {this.props.errors && this.props.errors.confirmPassword ? this.props.errors.confirmPassword.map((error, index) => <Text note style={{color: 'red'}} key={index}>{error}</Text>) : null}
                </Form>
                {this.props.errors && this.props.errors.global ? <Text style={{color: 'red', alignSelf: 'center'}}>{this.props.errors.global}</Text> : null}
            </View>
        )
    }
}

// eslint-disable-next-line no-class-assign
RegisterForm = reduxForm({
    form: 'registerForms',
    onSubmit: values => {
        const {email, password, confirmPassword} = values
        if (email && password && confirmPassword) {
            if (password === confirmPassword) {
                appStore.dispatch(register({email, password, confirmPassword}))
            } else {
                appStore.dispatch(setRegisterErrors({confirmPassword: ['password not identical']}))
            }

        } else {
            let errors = {}
            errors = email ? {...errors} : {...errors, email: ['Required Field']}
            errors = password ? {...errors} : {...errors, password: ['Required Field']}
            errors = confirmPassword ? {...errors} : {...errors, confirmPassword: ['Required Field']}
            appStore.dispatch(setRegisterErrors(errors))
        }
    },
}, null, {})(RegisterForm)


class Register extends React.Component {

    showSpinnerWhenProcessing = () => {
        if (this.props.registerProcessing) {

            return <View style={{alignItems: 'center', marginTop: 20}} >
                <Spinner size={40} type={'Wave'} color={'#ffc107'} />
            </View>
        } else {
            return null
        }
    }
    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#010101'}}>

                <Container>
                    <Content padder>
                        <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 100, marginBottom: 80}}>
                            <Image source={require('./logo.png')} style={{resizeMode: 'cover', position: 'absolute'}} />
                        </View>
                        <RegisterForm ref={'registerForm'} errors={this.props.registerError} />
                        {this.showSpinnerWhenProcessing()}
                        <Button block disabled={this.props.registerProcessing} warning style={this.props.registerProcessing ? {marginTop: 10} : {marginTop: 30}} warning onPress={() => this.refs.registerForm.submit()}>
                            <Text>Register</Text>
                        </Button>
                        <View style={{flexDirection: 'row', marginTop: 15, flex: 1, justifyContent: 'center'}}>
                            <Button transparent onPress={() => Actions.login()} >
                                <Text style={{color: '#FFF'}} >Login</Text>
                            </Button>
                        </View>
                    </Content>
                </Container>
            </View >

        )
    }
}
export default connect(({registerProcessing, registerError}) => ({registerProcessing, registerError}))(Register)
