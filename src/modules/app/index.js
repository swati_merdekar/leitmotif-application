import React from 'react'
import { Drawer, View } from 'native-base'
import { Router, Scene } from 'react-native-router-flux'

import User from '../../modules/user/components'
import Home from '../../modules/home/components'
import Register from '../../modules/register/components'
import Login from '../../modules/login/components'
import Greeting from '../../modules/greeting/components'


class App extends React.Component {



    componentDidUpdate() {
        if (this.props.drawerIsOpen) {
            this.drawer._root.open()
        }
    }


    closeDrawer = () => {
        this.drawer._root.close()
    }

    openDrawer = () => {
        this.drawer._root.open()
    }

    render() {
        const headerStyle = {
            backgroundColor: '#3f51b5',
            color: '#FFF',
        }
        return (

            <Drawer ref={ref => { this.drawer = ref }}
                content={<View style={{ flex: 1, backgroundColor: 'red' }}></View>}
                onClose={() => {
                    this.drawer._root.close()
                }}
                openDrawerOffset={0.5}
                panCloseMask={0.5}
            >
                <Router>
                    <Scene key="root">
                        <Scene
                            navigationBarStyle={{ backgroundColor: headerStyle.backgroundColor }}
                            titleStyle={{ color: headerStyle.color }}
                            key="home"
                            component={Home}
                            title="Home"
                            hideNavBar={true}

                        />
                        <Scene
                            navigationBarStyle={{ backgroundColor: headerStyle.backgroundColor }}
                            titleStyle={{ color: headerStyle.color }}
                            key="user"
                            component={User}
                            hideNavBar={false}
                            title="User"
                        />
                        <Scene
                            navigationBarStyle={{ backgroundColor: headerStyle.backgroundColor }}
                            titleStyle={{ color: headerStyle.color }}
                            key="register"
                            component={Register}
                            hideNavBar={true}
                            title="Register"


                        />
                        <Scene
                            navigationBarStyle={{ backgroundColor: headerStyle.backgroundColor }}
                            titleStyle={{ color: headerStyle.color }}
                            key="login"
                            component={Login}
                            hideNavBar={true}
                            title="Login"
                            initial
                        />
                        <Scene
                            navigationBarStyle={{ backgroundColor: headerStyle.backgroundColor }}
                            titleStyle={{ color: headerStyle.color }}
                            key="greeting"
                            component={Greeting}
                            hideNavBar={true}
                            title="greetin"
                        />
                    </Scene>
                </Router>
            </Drawer>

        )
    }
}

export default App
