import React from 'react'
import {View} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { ListItem, Text } from 'native-base'
import _ from 'lodash'
import { selectSlogan } from '../../../actions'
import { FONT_COLOR,BAKGRROUND_COLOR } from '../../../constants/color'


class SideBarItem extends React.Component {
    render() {
        return (
            <ListItem
                style={{backgroundColor:BAKGRROUND_COLOR}}
                selected={this.props.item.id === this.props.selectedSlogan.id}
                onPress={() => {
                    this.props.selectSlogan(this.props.item)
                    this.props.onChoise()
                }}>
                <View style={{ flex:1, flexDirection: 'row'}}>
                    <Text style={this.props.item.id === this.props.selectedSlogan.id ? {flex: 4, color: FONT_COLOR, fontWeight: 'bold', fontFamily: 'Bookman Old Style'} : {flex: 4, color: '#FFF',fontWeight: 'normal', fontFamily: 'Bookman Old Style' }}  >{_.truncate(this.props.item.text, { length: 20 })}</Text>
                    <Text style={this.props.item.id === this.props.selectedSlogan.id ? {flex: 1, color: FONT_COLOR, fontWeight: 'bold' } : {flex: 1, color: '#FFF',fontWeight: 'normal' }}  >{_.truncate(this.props.item.price, { length: 4 })}$</Text>
                </View>
                
            </ListItem>
        )
    }
}

SideBarItem.propTypes = {
    item: PropTypes.object.isRequired,
    onChoise: PropTypes.func.isRequired,
}

export default connect(({ selectedSlogan }) => ({ selectedSlogan }), { selectSlogan })(SideBarItem)

