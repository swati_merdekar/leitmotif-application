import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { MaterialDialog } from 'react-native-material-dialog'
import { StyleSheet, ListView } from 'react-native'
import PropTypes from 'prop-types'
import { Content, View, List, Button, Text } from 'native-base'
import {
    toggleFilicitationDialog, lastRequest, loadSlogans, pay
} from '../../../actions'
import { BAKGRROUND_COLOR, FONT_COLOR } from '../../../constants/color'
import CreateSloganDialog from '../../../components/create-slogan-dialog'
import SloganRequestDialog from '../../../components/slogan-request-dialog'
import SideBarItem from './sideBarItem'
import Orientation from 'react-native-orientation'


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        shadowOpacity: 0.8,
        shadowColor: '#FFFFFF',
        shadowRadius: 3,
        borderWidth: 1,
        borderLeftColor: '#ffc107',
    },
    textfieldWithFloatingLabel: {
        width: 200,
    },
    addButtonColor: {
        color: BAKGRROUND_COLOR,
    },
    addButton: {
        backgroundColor: FONT_COLOR,
        borderRadius: 0,
    },
})

class SideBar extends React.Component {
    constructor() {
        super()
        this.state = {
            dialogState: false
        }
    }


    componentDidMount() {
        this.props.loadSlogans()
        Orientation.lockToLandscape()
    }
 

    render() {
        console.log('+++++++++++++++++++++', this.props)

        const dsource = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1.id !== r2.id,
        })
        const slogans = this.props.slogans.slogans
        return (
            <View style={[styles.container, { width: this.props.width }]} onTouchStart={() => this.props.onStartTouched()} >
                <Button block style={styles.addButton} onPress={() => {
                    this.props.onClick()
                    this.props.lastRequest()
                    //this.props.pay(25)
                    
                }} >
                    <Text style={styles.addButtonColor} > Mine</Text>
                </Button>
                <Content style={{ backgroundColor: BAKGRROUND_COLOR }}>
                    <List dataArray={dsource.cloneWithRows(slogans.length === 0 ? [] : _.reverse(_.sortBy(slogans, 'id')))._dataBlob.s1}
                        renderRow={item => <SideBarItem onChoise={() => this.props.onChoise()} item={item} />}>
                    </List>
                </Content>
                <CreateSloganDialog />
                <SloganRequestDialog />
                <MaterialDialog
                    title={'Fongratulations you have created a new slogan'}
                    visible={this.props.felicitationDialogProcess}
                    onCancel={() => {
                        this.props.toggleFilicitationDialog()
                    }}>
                </MaterialDialog>
                


            </View >
        )
    }
}


SideBar.propTypes = {
    width: PropTypes.number.isRequired,
    onChoise: PropTypes.func.isRequired,
    onClick: PropTypes.func,
    onStartTouched: PropTypes.func,
}
// eslint-disable-next-line max-len
export default connect(({ felicitationDialogProcess, slogans }) =>
    ({ felicitationDialogProcess, slogans }), { toggleFilicitationDialog, lastRequest, loadSlogans, pay })(SideBar)

