import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Actions } from 'react-native-router-flux'
import { StyleSheet, Image } from 'react-native'
import {
    View, Container, Content, Text, Button, Form, Item, Input, Icon,
} from 'native-base'

import Spinner from 'react-native-spinkit'
import { login, setLoginErrors } from '../../../actions'
import { appStore } from '../../../'


class LoginForm extends React.Component {

    componentDidMount() {
        this.props.reset()
        appStore.dispatch(setLoginErrors(null))
    }

    renderField = ({ input }) => {
        return <Input secureTextEntry={input.name === 'password'}
            keyboardType={input.name === 'email' ? 'email-address' : 'default'}
            style={{ color: '#90a4ae', backgroundColor: '#011219' }} onChangeText={() => {
                if (this.props.errors) {
                    let errors = {}
                    for (const errorKey in this.props.errors) {
                        if (errorKey !== input.name) {
                            errors = { ...errors, [errorKey]: this.props.errors[errorKey] }
                        }

                    }
                    appStore.dispatch(setLoginErrors(errors))

                }
            }}  {...input } placeholder={`${input.name}`} />
    }
    // eslint-disable-next-line complexity
    render() {
        return (
            <View>
                <Form>
                    <Item error={this.props.errors && this.props.errors.email ? true : false} regular style={{ marginTop: 20 }} >
                        <Field name="email" component={this.renderField} />
                        {this.props.errors && this.props.errors.email ? <Icon name="close-circle" /> : null}
                    </Item>
                    {this.props.errors && this.props.errors.email ? this.props.errors.email.map((error, index) => <Text note style={{ color: 'red' }} key={index}>{error}</Text>) : null}
                    <Text></Text>
                    <Text></Text>
                    <Item error={this.props.errors && this.props.errors.password ? true : false} regular >
                        <Field name="password" component={this.renderField} />
                        {this.props.errors && this.props.errors.password ? <Icon name="close-circle" /> : null}
                    </Item>
                    {this.props.errors && this.props.errors.password ? this.props.errors.password.map((error, index) => <Text note style={{ color: 'red' }} key={index}>{error}</Text>) : null}
                </Form >
                {this.props.errors && this.props.errors.global ? <Text note style={{ color: 'red', alignSelf: 'center' }}>{this.props.errors.global}</Text> : null}
                {this.props.errors && this.props.errors.csrf_token ? <Text note style={{ color: 'red', alignSelf: 'center' }}>{this.props.errors.csrf_token}</Text> : null}
            </View>

        )
    }
}

// connect(({ loginErrors }) => ({ loginErrors }), )(LoginForm)


LoginForm = reduxForm({
    form: 'loginForms',
    onSubmit: values => {
        const { email, password } = values
        if (email && password) {
            appStore.dispatch(login({ email, password }))
        } else {
            const errors = {}
            if (!email) errors = { ...errors, email: ['Required Field'] }
            if (!password) errors = { ...errors, password: ['Required Field'] }
            appStore.dispatch(setLoginErrors(errors))
        }

    },
}, ({ loginErrors }) => ({ loginErrors }), {})(LoginForm)


class Login extends React.Component {

    showSpinnerWhenProcessing = () => {
        if (this.props.loginProcessing) {

            return <View style={{ alignItems: 'center', marginTop: 20 }} >
                <Spinner size={40} type={'Wave'} color={'#ffc107'} />
            </View>
        } else {
            return null
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#010101' }}>

                <Container>
                    <Content padder>
                        <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 80, marginBottom: 90 }}>
                            <Image source={require('./logo.png')} style={{ resizeMode: 'cover', position: 'absolute', }} />
                        </View>

                        <LoginForm ref={'loginForm'} errors={this.props.loginErrors} />

                        {this.showSpinnerWhenProcessing()}


                        <Button block warning disabled={this.props.loginProcessing} style={this.props.loginProcessing ? { marginTop: 10 } : { marginTop: 30 }} onPress={() => this.refs.loginForm.submit()} >

                            <Text>Login</Text>

                        </Button>
                        <View style={{ flexDirection: 'row', marginTop: 15, flex: 1 }}>
                            <View>
                                <Button transparent onPress={() => Actions.register()} >

                                    <Text style={{ color: '#FFF' }} >Register</Text>
                                </Button>
                            </View>
                            <View style={{ right: 0, position: 'absolute' }}>
                                <Button transparent onPress={() => Actions.forgotPassword()} >
                                    <Text style={{ color: '#FFF' }} >Forgot password?</Text>
                                </Button>
                            </View>
                        </View>
                    </Content>
                </Container>

            </View>

        )
    }
}

export default connect(({ loginProcessing, loginErrors }) => ({ loginProcessing, loginErrors }))(Login)
