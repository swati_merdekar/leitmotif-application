import React from 'react';
import {connect} from 'react-redux'
import {StyleSheet, View, Text,Button,TouchableHighlight} from 'react-native';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';
import StepIndicator from 'react-native-step-indicator';
import t from 'tcomb-form-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SloganRequestDialog from '../../../components/slogan-request-dialog'
import { BAKGRROUND_COLOR, FONT_COLOR } from '../../../constants/color'
import {showEditSloganDialog, deleteSloganRequest, pay} from '../../../actions'

var _ = require('lodash');
const Form = t.form.Form;
const stylesheet = _.cloneDeep(t.form.Form.stylesheet);

stylesheet.textbox.normal.borderWidth = 0;
stylesheet.textbox.error.borderWidth = 0;
stylesheet.textbox.normal.marginBottom = 0;
stylesheet.textbox.error.marginBottom = 0;
stylesheet.textbox.normal.color = 'white';
stylesheet.textboxView.normal.borderWidth = 0;
stylesheet.textboxView.error.borderWidth = 0;
stylesheet.textboxView.normal.borderRadius = 0;
stylesheet.textboxView.error.borderRadius = 0;
stylesheet.textboxView.normal.borderBottomWidth = 1;
stylesheet.textboxView.error.borderBottomWidth = 1;
stylesheet.textboxView.normal.borderColor=FONT_COLOR;
stylesheet.textboxView.error.borderColor=FONT_COLOR;

const firstIndicatorStyles = {
  stepIndicatorSize: 20,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 3,
  currentStepStrokeWidth: 5,
  separatorFinishedColor: '#4aae4f',
  separatorUnFinishedColor: '#ffffff',
  stepIndicatorFinishedColor: FONT_COLOR,
  stepIndicatorUnFinishedColor: FONT_COLOR,
  stepIndicatorCurrentColor: '#000000',
  stepIndicatorLabelFontSize: 15,
  currentStepIndicatorLabelFontSize: 15,
  stepIndicatorLabelCurrentColor: FONT_COLOR,
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: FONT_COLOR,
  labelColor: '#ffffff',
  labelSize: 12,
  currentStepLabelColor:FONT_COLOR
}

var navigate = false;
const Email = t.refinement(t.String, email => {
  const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/; //or any other regexp
  return reg.test(email);
});

const User = t.struct({
  firstName: t.String,
  secondName: t.String,
  address: t.String,
  email: Email,
  city_town: t.String
});
const address = t.struct({
  state: t.String,
  post: t.String,
  country: t.String,
  telephone: t.String
});

const options = {
  stylesheet: stylesheet,
  fields: {
    email: {
      error: 'Please enter valid email id',
      label:'Email-id:'
    },
    firstName: {
      error: 'Sorry, field firstName is required!',
      label:'First Name:'
    },
    secondName: {
      label:'Second Name:'
    },
    address: {
      error: 'Field address is required!',
      label:'Shippind Address:'
    },
    city_town: {
      label: 'City/Town',
      error: 'Field City Town is required!',
    },
  },
};
const options_address = {
  stylesheet: stylesheet,
  fields: {
    post: {
      error: 'Field Post is required!',
      label: 'Post/Zip:'
    },
    country: {
      error: 'Enter Country',
      label: 'Country:',
    },
    state: {
      error: 'Field State is required!',
      label:'State/region:'
    },
    telephone: {
      error: 'Field Telephone is required!',
      label:'Telephone:'
    }
  },
};
class Payment extends React.Component{
	constructor(props:TodoProps) {
	    super(props)
      console.log(this.props)
	    this.state = {
	     	 currentPage:0,
         ButtonStateHolder : false,
         formData:'',
         formData1:'',
	    }
      this.valueText;
      this.valueText1;
      this.onPageChange = this.onPageChange.bind(this);
      this.jsonObj = [];

  }
  onPress(){
        var value = this.refs.form.getValue();
        this.valueText1 = value;
        var value = this.refs.form1.getValue();
        console.log(this.refs.form);
        console.log(value);
        if(this.valueText1 && value ){
          this.navigate =true;
          this.valueText = value;
          console.log(this.valueText.state);
          this.onPageChange(1,value,this.valueText1);
          this.viewPager.setPage(1);
      }
  }
  onPressEdit(){
    this.viewPager.setPage(0);
    this.setState({
      currentPage:0
    })
  }
    onPageChange(position,value,value1){
      this.setState({  
        currentPage:position,
        ButtonStateHolder : true,
        formData:value, 
        formData1:value1
      })
      console.log(this.state.formData.state);
   }
   onPressPay(){
    this.createJSON()
    
    this.props.pay(this.props.editSlogan.data.price,this.jsonObj)
   }

    createJSON(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    this.jsonObj = [];
    item = {}
    item ["firstname"] = this.state.formData1.firstName;
    item ["secondname"] = this.state.formData1.secondName;
    item ["address"] = this.state.formData1.address;
    item ["email"] = this.state.formData1.email;
    item ["city_town"] = this.state.formData1.city_town;
    item ["state"] = this.state.formData.state;
    item ["post"] = this.state.formData.post;
    item ["country"] = this.state.formData.country;
    item ["telephone"] = this.state.formData.telephone;
    item ["time"] = dateTime;
    this.jsonObj.push(item);
    console.log(this.jsonObj);


  }

 render() {
        return (
<View style={styles.container}>
    <View style={styles.stepIndicator}>
      <StepIndicator 
      customStyles={firstIndicatorStyles} 
      currentPosition={this.state.currentPage} 
      stepCount={2}
      labels={['Delivery Address','Order Summary']} />
    </View>
    <View style={{flex:1}}>
      <IndicatorViewPager
      ref={viewPager => { this.viewPager = viewPager; }}
      style={{height:'100%'}}
      scrollEnabled = {this.state.ButtonStateHolder}
      indicator={this._renderDotIndicator()} 
      onPageSelected={(page) => {this.setState({currentPage:page.position})}}   
      >
        <KeyboardAwareScrollView style={{flex:1}} behavior="padding">
        <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.container}>
            <Form
            ref="form"
            type={User} 
            value={this.valueText1}
            options={options}
            />
            </View>
            <View style={styles.container}>
              <Form
              ref="form1"
              type={address} 
              options={options_address}
              value={this.valueText}
              />
              <TouchableHighlight style={styles.buttonBottom} onPress={this.onPress.bind(this)} underlayColor='#ffc107'>
                <Text style={styles.buttonText}>Confirm</Text>
              </TouchableHighlight>

            </View>
        </View>
        </KeyboardAwareScrollView>
       <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={styles.screen2}>
               <Text style={styles.textSub}>Delivery Address</Text>
               <Text style={styles.textTitle}>{this.state.formData1.firstName}  {this.state.formData1.secondName}</Text>
                <Text style={styles.textTitle}>{this.state.formData1.address}</Text>
                <Text style={styles.textTitle}>{this.state.formData.post}</Text>
                <Text style={styles.textTitle}>{this.state.formData1.city_town},{this.state.formData.state}</Text>
                <Text style={styles.textTitle}>{this.state.formData.country}</Text>
                <Text style={styles.textTitle}>Contact Number : {this.state.formData.telephone}</Text>
              <TouchableHighlight style={styles.buttonBottom} onPress={this.onPressEdit.bind(this)} underlayColor='#ffc107'>
                <Text style={styles.buttonText}>Edit</Text>
                </TouchableHighlight>
              </View>
              <View style={styles.container}>
                <Text style={styles.textSub}>Your Leitmotif:</Text>
                <Text style={styles.textSub}>Total Order  $ {this.props.editSlogan.data.price} </Text>
                <TouchableHighlight style={styles.buttonBottom} onPress={this.onPressPay.bind(this)} underlayColor='#ffc107'>
                <Text style={styles.buttonText}>Place your order</Text>
                </TouchableHighlight>
              </View>        
            </View> 
      </IndicatorViewPager>
    </View>
</View>
        );
    }
  _renderDotIndicator() {
      return <PagerDotIndicator pageCount={2} />;
  }

}
const styles = StyleSheet.create({
  container: {
    padding:'1%',
    flex: 1,
    backgroundColor:BAKGRROUND_COLOR,
  },
  stepIndicator: {
    marginVertical:0,
  },
  page: {
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  buttonText:{
    color:'black',
  },
    button: {
    height: 36,
    alignItems:'center',
    backgroundColor:FONT_COLOR,
    borderColor: FONT_COLOR,
    justifyContent: 'center'
  },
    buttonBottom: {
    height: 36,
    alignItems:'center',
    backgroundColor:FONT_COLOR,
    borderColor: FONT_COLOR,
    justifyContent: 'center',
    width: '80%', 
    position: 'absolute',
    marginLeft:'10%',
    bottom: 0
  },
  textSub:{
    color:FONT_COLOR,
    textAlign:'center',
    marginBottom:'10%',
    marginTop:'15%',
    fontWeight:'bold'
  },
  textTitle:{
    color:FONT_COLOR,
    textAlign:'center',
    fontSize: 15,
  },
  screen2:{
    padding:'1%',
    flex: 1,
    backgroundColor:BAKGRROUND_COLOR,
  },
  addButton: {
    backgroundColor: FONT_COLOR,
    borderRadius: 0,
    color:BAKGRROUND_COLOR,
    },
  addButtonColor: {
    color: BAKGRROUND_COLOR,
    }
});

export default connect(({editSlogan}) => ({editSlogan}),{deleteSloganRequest,showEditSloganDialog,pay})(Payment)