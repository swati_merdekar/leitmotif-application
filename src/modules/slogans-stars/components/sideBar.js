import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { StyleSheet, ListView } from 'react-native'
import PropTypes from 'prop-types'
import { Content, View, List, Button, Text } from 'native-base'
import { loadSlogansStars, lastRequest } from '../../../actions'
import { BAKGRROUND_COLOR, FONT_COLOR } from '../../../constants/color'
import CreateSloganDialog from '../../../components/create-slogan-dialog'
import SloganRequestDialog from '../../../components/slogan-request-dialog'
import SideBarItem from './sideBarItem'
import Orientation from 'react-native-orientation'

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        shadowOpacity: 0.8,
        shadowColor: '#FFFFFF',
        shadowRadius: 3,
        borderWidth: 1,
        borderLeftColor: '#ffc107',
    },
    addButtonColor: {
        color: BAKGRROUND_COLOR,
    },
    addButton: {
        backgroundColor: FONT_COLOR,
        borderRadius: 0,
    },
})


class SideBar extends React.Component {
    componentDidMount() {
        Orientation.lockToLandscape()
        this.props.loadSlogansStars()
    }

    render() {
        const dsource = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1.id !== r2.id,
        })
        const slogansStars = this.props.slogans.slogansStars
        return (
            <View style={[styles.container, { width: this.props.width }]} onTouchStart={() => this.props.onStartTouched()} >
                <Content style={{ backgroundColor: BAKGRROUND_COLOR }}>
                    <List dataArray={dsource.cloneWithRows(slogansStars.length === 0 ? [] : _.reverse(_.sortBy(slogansStars, 'id')))._dataBlob.s1}
                        renderRow={item => <SideBarItem onChoise={() => this.props.onChoise()} item={item} />}>
                    </List>
                </Content>
                <Button block style={styles.addButton} onPress={() => {
                    this.props.onClick()
                    this.props.lastRequest()
                }} >
                    <Text style={styles.addButtonColor} >create slogan</Text>
                </Button>
                <CreateSloganDialog />
                <SloganRequestDialog />
            </View >
        )
    }
}


SideBar.propTypes = {
    width: PropTypes.number.isRequired,
    onChoise: PropTypes.func.isRequired,
    onClick: PropTypes.func,
    onStartTouched: PropTypes.func,
}
export default connect(({ slogans }) => ({ slogans }), { loadSlogansStars,lastRequest })(SideBar)