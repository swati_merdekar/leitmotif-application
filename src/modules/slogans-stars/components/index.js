
import React from 'react'
import { connect } from 'react-redux'
import Orientation from 'react-native-orientation'
import {
    StyleSheet, Animated, Easing, Dimensions, TouchableOpacity, View,
} from 'react-native'
import {
    Text, Icon, Button, Drawer,
} from 'native-base'
import _ from 'lodash'
import NavigateList from '../../../components/navigate-list'
import Slogan from '../../../components/slogan'
import { BAKGRROUND_COLOR, FONT_COLOR } from '../../../constants/color'
import { shareSlogan, selectSloganStar } from '../../../actions'
import Sidebar from './sideBar'




const styles = StyleSheet.create({
    constainer: {
        flex: 1, flexDirection: 'row',
    },
    sloganPanel: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: BAKGRROUND_COLOR,
    },
    shareFab: {
        backgroundColor: FONT_COLOR,
        width: 60,
        height: 60,
        borderRadius: 100,
        bottom: 20,
        right: 20,
        position: 'absolute',
    },
    shareFabIconColor: {
        color: BAKGRROUND_COLOR,
    },
    date: {
        zIndex: 50,
        color: FONT_COLOR,
        top: 11,
        left: 45,
        position: 'absolute',
    },
    price: {
        zIndex: 50,
        color: FONT_COLOR,
        top: 11,
        right: 10,
        position: 'absolute',
    },
})


class SlogansStars extends React.Component {
    constructor() {
        super()
        this.state = {
            isDispaleyed: false,
            selectedSlogan: false,
            fontSize: 20,
            currentTimeOut: NaN,
            activeFab: false,
            addNewSloganDialog: false,
        }
        this.spinValue = new Animated.Value(0)
        this.badgeValue = new Animated.Value(0)
        this.fabValue = new Animated.Value(0)
        this._shareButton = this._shareButton.bind(this)
        this.closeDrawer = this.closeDrawer.bind(this)
        this.openDrawer = this.openDrawer.bind(this)
    }

    componentDidMount() {
        Orientation.addOrientationListener(() => Orientation.lockToLandscape());   
        this.props.selectSloganStar({})
        clearTimeout(this.state.currentTimeOut)
        const currentTimeOut = setTimeout(() => {
            this.setState({ isDispaleyed: !this.state.isDispaleyed })
        }, 7000)
        this.setState({ currentTimeOut })
    }

    componentDidUpdate() {
        if (this.state.isDispaleyed) {
            this.spinValue.setValue(0)
            Animated.timing(this.spinValue, {
                toValue: 1,
                duration: 500,
                easing: Easing.cubic,
            }).start()
            this.badgeValue.setValue(0)
            Animated.timing(this.badgeValue, {
                toValue: 1,
                duration: 500,
                easing: Easing.cubic,
            }).start()

            this.fabValue.setValue(0)
            Animated.timing(this.fabValue, {
                toValue: 1,
                duration: 500,
                easing: Easing.cubic,
            }).start()
        } else {

            Animated.timing(this.spinValue, {
                toValue: 0,
                duration: 500,
                easing: Easing.cubic,
            }).start()
            Animated.timing(this.badgeValue, {
                toValue: 0,
                duration: 500,
                easing: Easing.cubic,
            }).start()
            Animated.timing(this.fabValue, {
                toValue: 0,
                duration: 500,
                easing: Easing.cubic,
            }).start()
        }

        if (!_.isEqual(this.props.slogans.slogansStars, []) && _.isEqual(this.props.selectedSloganStar, {})) {
            this.props.selectSloganStar(_.last(this.props.slogans.slogansStars))
        }
    }
    _shareButton() {
        const badgeOpacity = this.fabValue.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0],
        })
        const ShareFab = Animated.createAnimatedComponent(TouchableOpacity)
        if (this.props.selectedSloganStar.price) {
            return (
                <ShareFab activeOpacity={0.5} onPress={() => {
                    clearTimeout(this.state.currentTimeOut)
                    this.props.shareSlogan(this.refs.sloganNode)
                }} style={[styles.shareFab, { opacity: badgeOpacity, justifyContent: 'center', alignItems: 'center' }]}>
                    <Icon name="share" style={styles.shareFabIconColor} />
                </ShareFab>
            )
        } else {
            return null
        }
    }

    closeDrawer = () => {
        this.drawer._root.close()
    }
    openDrawer = () => {
        this.drawer._root.open()
    }

    formatingDate(dateString) {
        if (dateString) {
            const date = new Date(dateString)
            const month = date.getMonth() + 1
            const day = date.getDate()
            const year = date.getFullYear()
            return `${day}/${month}/${year}`
        }
        return null
    }

    render() {
        const { height, width } = Dimensions.get('window')
        const currentWidth = height > width ? height : width
        const sloganPanelWidth = (currentWidth - currentWidth * 30 / 100)
        const sideBarWidth = (currentWidth * 30 / 100)

        const sloganPanelWidths = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: [sloganPanelWidth, width],
        })
        const badgeOpacity = this.badgeValue.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0],
        })
        const SloganAnimation = Animated.createAnimatedComponent(Slogan)
        const MainView = Animated.createAnimatedComponent(TouchableOpacity)
        const TextAnimation = Animated.createAnimatedComponent(Text)
        const IconAnimation = Animated.createAnimatedComponent(Icon)
        return (

            <Drawer ref={ref => { this.drawer = ref }}
                content={<NavigateList />}
                onClose={() => this.closeDrawer()}
                openDrawerOffset={0.5}
                panCloseMask={0.5}
            >
                <View style={styles.constainer}>
                    <MainView
                        activeOpacity={1}
                        onPress={() => {
                            clearTimeout(this.state.currentTimeOut)
                            if (this.state.isDispaleyed) {
                                const currentTimeOut = setTimeout(() => {
                                    this.setState({ isDispaleyed: !this.state.isDispaleyed })
                                }, 7000)
                                this.setState({ currentTimeOut })
                            }
                            this.setState({ isDispaleyed: !this.state.isDispaleyed, selectedSlogan: false })
                        }}

                        style={[styles.sloganPanel, { width: sloganPanelWidths }]}
                    >
                        <Button iconLeft style={{ position: 'absolute', top: 0, left: 0, zIndex: 50 }} transparent onPress={() => {
                            if (!this.state.isDispaleyed) {
                                this.openDrawer()
                            }
                        }}>
                            <IconAnimation style={{ opacity: badgeOpacity, color: FONT_COLOR }} name="menu" />
                        </Button>
                        <TextAnimation style={[styles.date, { opacity: badgeOpacity }]}>
                            {this.formatingDate(this.props.selectedSloganStar.created_date)}
                        </TextAnimation>
                        <TextAnimation style={[styles.price, { opacity: badgeOpacity }]}>
                            {this.props.selectedSloganStar.price} {this.props.selectedSloganStar.price ? '$' : ''}
                        </TextAnimation>

                        <SloganAnimation
                            ref={'sloganNode'}
                            selected={this.state.selectedSlogan}
                            slogan={this.props.selectedSloganStar.text}
                            pseudo={this.props.selectedSloganStar.pseudo ? this.props.selectedSloganStar.pseudo.text : ''}
                            sloganStyles={{ fontSize: 30 }}
                            pseudoStyles={{ fontSize: 20 }}
                        />
                        {this._shareButton()}

                    </MainView>
                    <Sidebar
                        onChoise={() => {
                            this.setState({ selectedSlogan: true })
                            setTimeout(() => {
                                this.setState({ selectedSlogan: false })
                            }, 1)
                            this.setState({ fontSize: this.state.fontSize === 20 ? 40 : 20 })
                            clearTimeout(this.state.currentTimeOut)
                            const currentTimeOut = setTimeout(() => {
                                this.setState({ isDispaleyed: !this.state.isDispaleyed })
                            }, 7000)
                            this.setState({ currentTimeOut })
                        }}
                        onStartTouched={() => {
                            clearTimeout(this.state.currentTimeOut)
                            const currentTimeOut = setTimeout(() => {
                                this.setState({ isDispaleyed: !this.state.isDispaleyed })
                            }, 7000)
                            this.setState({ currentTimeOut })
                        }}
                        onClick={() => clearTimeout(this.state.currentTimeOut)}
                        width={sideBarWidth}
                    />
                </View >
            </Drawer >
        )
    }
}
// eslint-disable-next-line max-len
export default connect(({ selectedSloganStar, slogans }) => ({ selectedSloganStar, slogans }), { shareSlogan, selectSloganStar })(SlogansStars)
