import React from 'react'
import { } from 'react-native'
import { Container, Content, View, Text, Form, Item, Button, Input, Icon } from 'native-base'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import Spinner from 'react-native-spinkit'
import { forgotPassword, setForgotPasswordErrors } from '../../../actions'
import { appStore } from '../../../'
class ForgotPasswordForm extends React.Component {

    componentDidMount() {
        this.props.reset()
        appStore.dispatch(setForgotPasswordErrors(null))
    }
    renderField = ({ input }) => {
        return <Input keyboardType="email-address" style={{ color: '#90a4ae', backgroundColor: '#011219' }}
            {...input } placeholder={`${input.name}`}
        />
    }
    render() {
        return (
            <View>
                <Form>
                    <Item error={(this.props.errors && this.props.errors.email) ? true : false} regular style={{ marginTop: 20 }}>
                        <Field name="email" component={this.renderField} />
                        {this.props.errors && this.props.errors.email ? <Icon name="close-circle" /> : null}
                    </Item>
                    {this.props.errors && this.props.errors.email ? this.props.errors.email.map((error, index) => <Text note style={{ color: 'red' }} key={index}>{error}</Text>) : null}


                </Form>
            </View>
        )
    }
}

ForgotPasswordForm = reduxForm({
    form: 'forgotPasswordForms',
    onSubmit: values => {
        const { email } = values
        if (email) {
            appStore.dispatch(forgotPassword(email))
        } else {
            appStore.dispatch(setForgotPasswordErrors({ email: ['Required Field'] }))
        }

    }
}, null, {})(ForgotPasswordForm)




class ForgotPassword extends React.Component {
    showSpinnerWhenProcessing = () => {
        if (this.props.forgotPassword.isProcessing) {
            return <View style={{ alignItems: 'center', marginTop: 20 }} >
                <Spinner size={40} type={'Wave'} color={'#ffc107'} />
            </View>
        } else {
            return null
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#010101' }}>
                <Container>
                    <Content padder>
                        <Text style={{ color: '#009688' }}>{this.props.forgotPassword.success}</Text>
                        {this.props.forgotPassword.errors && this.props.forgotPassword.errors.global ? <Text style={{ color: 'red' }} >{this.props.forgotPassword.errors.global}</Text> : null}
                        <ForgotPasswordForm errors={this.props.forgotPassword.errors} ref={'forgotPasswordForm'} />
                        {this.showSpinnerWhenProcessing()}
                        <Button block warning disabled={this.props.forgotPassword.isProcessing} style={this.props.forgotPassword.isProcessing ? { marginTop: 10 } : { marginTop: 30 }} onPress={() => this.refs.forgotPasswordForm.submit()} >
                            <Text>Send Email</Text>
                        </Button>
                        <View style={{ flexDirection: 'row', marginTop: 15, flex: 1 }}>
                            <View>
                                <Button transparent onPress={() => Actions.register()} >
                                    <Text style={{ color: '#FFF' }} >Register</Text>
                                </Button>
                            </View>
                            <View style={{ right: 0, position: 'absolute' }}>
                                <Button transparent onPress={() => Actions.login()} >
                                    <Text style={{ color: '#FFF' }} >Login</Text>
                                </Button>
                            </View>
                        </View>
                    </Content>
                </Container>
            </View>
        )
    }
}

export default connect(({ forgotPassword }) => ({ forgotPassword }))(ForgotPassword)