// eslint-disable-next-line max-lines
import { AsyncStorage, Alert } from "react-native";
import PayPal from "react-native-paypal-wrapper";
import { takeSnapshot } from "react-native-view-shot";
import RNFS from "react-native-fs";
import Share from "react-native-share";
import axios from "axios";
import { Actions } from "react-native-router-flux";

import {
  SELECTED_SLOGAN,
  LOAD_USER,
  BUY_SLOGAN,
  SHARE_SLOGAN,
  CREATE_SLOGAN,
  TOGGLE_FELICITATION_DIALOG,
  REGISTER,
  REGISTER_PROCESS,
  LOGIN,
  LOGIN_PROCESS,
  TOKEN_STORAGE_KEY,
  LOGOUT,
  LOGIN__ERRORS,
  REGISTER_ERRORS,
  UPDATE_SESSION,
  UPDATE_PERSIST,
  SELECTED_SLOGAN_STAR,
  UPDATE_SLOGAN_FORM,
  FORGOT_PASSWORD_ERRORS,
  FORGOT_PASSWORD_PROCESSING,
  FORGOT_PASSWORD_MESSAGE_SUCCESS,
  CREATE_SLOGAN_PROCESSING,
  ADD_SLOGAN,
  LAST_REQUEST,
  EDIT_SLOGAN_DIALOG,
  MAKE_SLOGAN_REQUEST,
  DELETE_SLOGAN_REQUEST,
  HIGH_PAYMENT,
  LOAD_SLOGANS,
  LOAD_SLOGANS_STARTS
} from "../constants";
import { BASE_URL, API_ENDPOINT } from "../constants/api";
import { CLIENT_ID } from "../constants/config";
import { appStore } from "../";
import { initialState } from "../reducers/edit-slogan";
import { displayApiErrors } from "./handle-api-errors";

export function selectSlogan(data) {
  return {
    type: SELECTED_SLOGAN,
    payload: data
  };
}

export function selectSloganStar(data) {
  return {
    type: SELECTED_SLOGAN_STAR,
    payload: data
  };
}

export function addSlogan(data) {
  const slogan = {
    price: data.price,
    // eslint-disable-next-line camelcase
    created_date: data.date,
    text: data.slogan,
    pseudo: {
      text: data.pseudo
    }
  };
  if (Actions.currentScene !== "home") {
    Actions.home();
  }
  appStore.dispatch(createSloganProcessing(false));
  appStore.dispatch(selectSlogan(slogan));
  appStore.dispatch(toggleFilicitationDialog());
  return {
    type: ADD_SLOGAN,
    payload: slogan
  };
}

export function loadUser() {
  const token = appStore.getState().session.token.value;
  const userId = appStore.getState().session.user.id;
  const request = axios({
    method: "GET",
    url: `${API_ENDPOINT}users/${userId}`,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: token
    }
  });

  return {
    type: LOAD_USER,
    payload: request
  };
}

// export function loadUser(data) {
//     return {
//         type: LOAD_USER,
//         payload: data,
//     }
// }

export function pay(price,AddData) {
  console.log("------------pey");
  return dispatch => {
    // 3 env available: NO_NETWORK, SANDBOX, PRODUCTION
    PayPal.initialize(PayPal.SANDBOX, CLIENT_ID);
    console.log("-----pay: ", price);
    PayPal.pay({
      price: price.toString(),
      currency: "USD",
      description: "payment for leitmotif artwork"
    })
      .then(result => {
        console.log("++++", result);
        if (result.response) {
          dispatch(createSlogan(result.response.id,AddData));
        }
      })
      .catch(error => {
        console.log("++++", error);
        displayApiErrors(error.response);
      });
  };
}

export function createSlogan(paymentId,addData) {
  return (dispatch, getState) => {
    dispatch(createSloganProcessing(true));
    dispatch(selectSlogan({}));

    const token = getState().session.token.value;
    axios({
      method: "POST",
      url: `${API_ENDPOINT}slogans`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token
      },
      data: {
        payment: {
          key: paymentId
        }
      }
    })
      .then(response => {
        if (response.data) {
          const slogan = {
            pseudo: response.data.pseudo.text,
            slogan: response.data.text,
            price: response.data.price,
            date: response.data.created_date
          };

          dispatch(addSlogan(slogan));
          dispatch(deleteSloganRequest(appStore.getState().editSlogan.data.id));
          dispatch(showEditSloganDialog({ ...initialState, newDialog: false }));
          dispatch(addressDetails(addData));
        }
      })
      .catch(error => {
        appStore.dispatch(createSloganProcessing(false));
        displayApiErrors(error.response);
      });
  };
}

export function createSlogans(data) {
  const slogans = appStore.getState().user.slogans;
  const newSlogan = {
    id: slogans.length + 1,
    pseudo: data.pseudo,
    slogan: data.slogan,
    price: data.price,
    date: data.date
  };
  slogans.push(newSlogan);
  return {
    type: CREATE_SLOGAN
  };
}

export function shareSlogan(node) {
  const picturePath = `${RNFS.CachesDirectoryPath}/slogan_sharing.png`;

  takeSnapshot(node, { path: picturePath }).then(
    () => {
      RNFS.exists(picturePath).then(result => {
        if (result) {
          RNFS.readFile(picturePath, "base64").then(base64 => {
            const shareImageBase64 = {
              title: "leitmotif",
              message: "cette image est partagé par leitmotif",
              url: `data:image/png;base64,${base64}`
            };
            Share.open(shareImageBase64).then(r => {
              if (r.message === "OK") {
                RNFS.unlink(picturePath)
                  .then(() => {
                    console.log("FILE DELETED");
                  })
                  .catch(err => {
                    console.log(err);
                  });
              }
            });
          });
        } else {
          console.log("le ficher n'est pas trouve ");
        }
      });
    },
    error => console.error(`Oops, snapshot failed`, error)
  );
  return {
    type: SHARE_SLOGAN
  };
}

export function toggleFilicitationDialog() {
  return {
    type: TOGGLE_FELICITATION_DIALOG
  };
}

export function register({ email, password, confirmPassword }) {
  const data = {
    email,
    password,
    // eslint-disable-next-line camelcase
    password_confirm: confirmPassword,
    role: "user",
    // eslint-disable-next-line camelcase
    first_name: "user_f",
    // eslint-disable-next-line camelcase
    last_name: "user_l",
    accept: true
  };
  return dispatch => {
    dispatch(registerProcessing(true));
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    axios
      .post(`${BASE_URL}register`, data)
      .then(response => {
        if (!response.data.response.errors) {
          const userAuthenticated = {
            token: {
              value: response.data.response.user.authentication_token
            },
            user: {
              id: response.data.response.user.id
            }
          };
          AsyncStorage.setItem(
            TOKEN_STORAGE_KEY,
            JSON.stringify(userAuthenticated)
          ).then(() => {
            console.log("sucess");
          });
          dispatch(updateSession(userAuthenticated));
          Actions.slogansStars();
        } else if (response.data.response.errors) {
          dispatch(setRegisterErrors(response.data.response.errors));
        }
        dispatch(registerProcessing(false));
      })
      .catch(error => {
        dispatch(registerProcessing(false));
        if (error.response.status === "404") {
          dispatch(setRegisterErrors({ global: "service not available" }));
        } else {
          displayApiErrors(error.response);
        }
      });
  };
}

export function registerProcessing(data) {
  return {
    type: REGISTER_PROCESS,
    payload: data
  };
}

export function login({ email, password }){
  return dispatch => {
    dispatch(loginProcessing(true));
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    axios
      .post(`${BASE_URL}login`, { email, password })
      .then(response => {
        axios.get(`${BASE_URL}logout`).then(res => {
          console.log(res)
        })
        if (!response.data.response.errors) {
          const userAuthenticated = {
            token: {
              value: response.data.response.user.authentication_token
            },
            user: {
              id: response.data.response.user.id
            }
          };
          AsyncStorage.setItem(
            TOKEN_STORAGE_KEY,
            JSON.stringify(userAuthenticated)
          ).then(() => {
            console.log("success");
          });
          dispatch(updateSession(userAuthenticated));
          Actions.slogansStars();
        } else if (response.data.response.errors) {
          dispatch(setLoginErrors(response.data.response.errors));
        }
        dispatch(loginProcessing(false));
      })
      .catch(error => {
          console.log('-----',error.response)
           dispatch(loginProcessing(false));
        if (error.response && error.response.status === "404") {    
          dispatch(setLoginErrors({ global: "Service not available" }));
        } else if (!error.response) {
          console.log(`uncomprehisble error ${error}`);
        } else {
          displayApiErrors(error.response);
        }
      });
  };
}

export function loginProcessing(data){
  return {
    type: LOGIN_PROCESS,
    payload: data
  };
}

export function logout() {
  return dispatch => {
    AsyncStorage.removeItem(TOKEN_STORAGE_KEY)
      .then(token => {
        const userAuthenticated = {
          token: {
            value: null
          },
          user: {
            id: null
          }
        };
        dispatch(updateSession(userAuthenticated));
        Actions.login();
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function setLoginErrors(data) {
  return {
    type: LOGIN__ERRORS,
    payload: data
  };
}

export function setRegisterErrors(data) {
  return {
    type: REGISTER_ERRORS,
    payload: data
  };
}

export function lastRequest() {
  return (dispatch, getState) => {
    const token = getState().session.token.value;
    axios({
      method: "GET",
      url: `${API_ENDPOINT}request_historys`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token
      }
    })
      .then(response => {
        if (response.data.length === 0) {
          dispatch(
            showEditSloganDialog({
              isExist: false,
              newDialog: true
            })
          );
        } else {
          dispatch(
            showEditSloganDialog({
              isExist: true,
              requestDialog: true,
              data: { ...response.data[0] }
            })
          );
        }
      })
      .catch(error => {
        if (error.response) {
          displayApiErrors(error.response);
        } else {
          displayApiErrors(error);
        }
      });
  };
}

export function showEditSloganDialog(data) {
  return {
    type: EDIT_SLOGAN_DIALOG,
    payload: data
  };
}

export function makeSloganRequest(data) {
  return (dispatch, getState) => {
    const token = getState().session.token.value;
    axios({
      method: "POST",
      url: `${API_ENDPOINT}request_historys`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token
      },
      data: {
        slogan: data.slogan,
        pseudo: data.pseudo
      }
    })
      .then(response => {
        if (response.data) {
          dispatch(
            showEditSloganDialog({
              isExist: true,
              newDialog: false,
              data: { ...response.data }
            })
          );
          dispatch(updateSloganForm({ errors: null }));
          Actions.payment();
        }
      })
      .catch(error => {
        if (error.response.data.form_errors) {
          dispatch(
            updateSloganForm({
              errors: { ...error.response.data.form_errors }
            })
          );
        } else {
          displayApiErrors(error.response);
        }
      });
  };
}

export function addressDetails(data){
  return (dispatch, getState) => {
    const token = getState().session.token.value;
    axios({
      method: "POST",
      url: `${API_ENDPOINT}tests`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token
      },
      data: data[0]
    })
      .then(response => {
        if (response.data) {
          console.log(response);
        }
      })
      .catch(error => {
        if (error.response) {
          dispatch(
            updateSloganForm({
              errors: { ...error.response }
            })
          );
        } else {
          displayApiErrors(error.response);
        }
      });
  };
}


export function deleteSloganRequest(id) {
  return (dispatch, getState) => {
    const token = getState().session.token.value;
    axios({
      method: "DELETE",
      url: `${API_ENDPOINT}request_historys/${id}`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token
      }
    })
      .then(response => {
        console.log("slogan deleted");
      })
      .catch(error => displayApiErrors(error.response));
  };
}

export function getHighPayment() {
  return {
    type: HIGH_PAYMENT
  };
}

export function updateSession(session) {
  return {
    type: UPDATE_SESSION,
    session
  };
}

export function updatePersist(payload) {
  return {
    type: UPDATE_PERSIST,
    payload
  };
}

export function loadSlogans() {
  const token = appStore.getState().session.token.value;
  const userId = appStore.getState().session.user.id;

  const request = axios({
    method: "GET",
    url: `${API_ENDPOINT}slogans?user_id=${userId}`,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: token
    }
  });
  return {
    type: LOAD_SLOGANS,
    payload: request
  };
}

export function loadSlogansStars() {
  const token = appStore.getState().session.token.value;
  const userId = appStore.getState().session.user.id;
  const request = axios({
    method: "GET",
    url: `${API_ENDPOINT}slogans?ne_user_id=${userId}`,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: token
    }
  });
  return {
    type: LOAD_SLOGANS_STARTS,
    payload: request
  };
}

export function updateSloganForm(payload) {
  return {
    type: UPDATE_SLOGAN_FORM,
    payload
  };
}

export function forgotPasswordProcessing(payload) {
  return {
    type: FORGOT_PASSWORD_PROCESSING,
    payload
  };
}

export function setForgotPasswordErrors(payload) {
  return {
    type: FORGOT_PASSWORD_ERRORS,
    payload
  };
}
export function setForgotPasswordMessageSuccess(payload) {
  return {
    type: FORGOT_PASSWORD_MESSAGE_SUCCESS,
    payload
  };
}
export function forgotPassword(email) {
  return dispatch => {
    dispatch(forgotPasswordProcessing(true));
    axios({
      method: "POST",
      url: `${BASE_URL}reset`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      data: {
        email
      }
    })
      .then(response => {
        dispatch(forgotPasswordProcessing(false));
        if (response.data.response.errors) {
          dispatch(setForgotPasswordErrors(response.data.response.errors));
        } else {
          dispatch(
            setForgotPasswordMessageSuccess("Reset email has been sent.")
          );
        }
      })
      .catch(() => {
        dispatch(forgotPasswordProcessing(false));
        dispatch(setForgotPasswordErrors({ global: "Service not available" }));
      });
  };
}

export function createSloganProcessing(payload) {
  return {
    type: CREATE_SLOGAN_PROCESSING,
    payload
  };
}
