
import { Alert } from 'react-native'
import _ from 'lodash'

export function displayErrors(errors, fieldsLabels = {}) {
    let errorMessage = ''
    for (const key of Object.keys(errors)) {
       
        if (_.isPlainObject(errors[key])) {
            errorMessage = `${errorMessage} ${key}: \n`
            for (const k of Object.keys(errors[key])) {
                errorMessage = `${errorMessage}     ${k}: \n`
                for (const value of errors[key][k]) {
                    errorMessage = `${errorMessage}          ${value}\n`
                }
            }
           
        } else {
            errorMessage = `${errorMessage} ${key}: \n`
            for (const value of errors[key]) {
                errorMessage = `${errorMessage}     ${value}\n`
            }
        }
        
        
    }
    Alert.alert('error', errorMessage)
   

}


export function displayApiErrors(errors, fields = {}) {

    switch (errors.status) {
        case 400:
            displayErrors(errors.data.form_errors, fields)
            break
        case 401:
            Alert.alert('error', 'You do not have the right to do this operation.')
            break
        default:
            Alert.alert('Internal error, please contact the admin.')
    }
}
