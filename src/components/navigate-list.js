import React from 'react'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { Image } from 'react-native'
import {
    View, List, ListItem, Icon, Body, Left, Text,
} from 'native-base'
import { BAKGRROUND_COLOR, FONT_COLOR } from '../constants/color'
import { logout, loadUser } from '../actions'
import { appStore } from '../'

class NavigateList extends React.Component {

    componentDidMount() {
        this.props.loadUser()
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFF', borderRightWidth: 1, borderRightColor: FONT_COLOR }}>
                <View style={{ backgroundColor: BAKGRROUND_COLOR, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Icon style={{ color: FONT_COLOR, fontSize: 70, marginLeft: 10 }} name="contact" />
                    {this.props.user ? <Text style={{ color: FONT_COLOR, marginLeft: 10, fontSize: 15 }} >{this.props.user.email}</Text> : null}

                    {/*<Image source={require('./l2.png')} style={{resizeMode:'cover',alignSelf:'center'}} />   */}
                </View>
                <View style={{ flex: 2 }}>
                    <List >
                        <ListItem icon onPress={() => {
                            if (Actions.currentScene !== 'home') {
                                Actions.home()
                            }
                        }}>
                            <Left>
                                <Icon name="person" />
                            </Left>
                            <Body>
                                <Text style={Actions.currentScene === 'home' ? { fontWeight: 'bold' } : {}}>My slogans</Text>
                            </Body>
                        </ListItem>

                        <ListItem icon onPress={() => {
                            if (Actions.currentScene !== 'slogansStars') {
                                Actions.slogansStars()
                            }
                        }}>
                            <Left>
                                <Icon name="ribbon" />
                            </Left>
                            <Body>
                                <Text style={Actions.currentScene === 'slogansStars' ? { fontWeight: 'bold' } : {}} >Star slogans</Text>
                            </Body>
                        </ListItem>
                        <ListItem icon onPress={() => appStore.dispatch(logout())} >
                            <Left>
                                <Icon name="log-out" />
                            </Left>
                            <Body>
                                <Text>Logout</Text>
                            </Body>
                        </ListItem>
                    </List>
                </View>

            </View>
        )
    }
}
export default connect(({ user }) => ({ user }), { loadUser })(NavigateList)
