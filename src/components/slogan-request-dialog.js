import React from 'react'
import {connect} from 'react-redux'
import {batchActions} from 'redux-batched-actions'
import {MaterialDialog} from 'react-native-material-dialog'
import {View, Text} from 'native-base'
// eslint-disable-next-line import/no-extraneous-dependencies
import ReactNativeComponentTree from 'ReactNativeComponentTree'
import {appStore} from '../'
import {initialState} from '../reducers/edit-slogan'
import {showEditSloganDialog, deleteSloganRequest, pay} from '../actions'
import PopupDialog from 'react-native-popup-dialog'
import { Actions } from 'react-native-router-flux'
class SloganRequestDialog extends React.Component {
    render() {
        console.log(this.props)
        return (
            <MaterialDialog
                title={'You already have a request'}
                visible={this.props.editSlogan.requestDialog}
                okLabel="Pay this slogan"
                cancelLabel="Delete and create again"

                onOk={() => {
                    console.log('----- response ok',)
                        this.props.showEditSloganDialog({requestDialog: false})
                        /*
                        setTimeout(()=>{
                            this.props.pay(this.props.editSlogan.data.price)
                        },1000)*/ // redirect to payment screen 
                        setTimeout(()=>{
                           Actions.payment()
                        },1000)

                }}
                onCancel={e => {
                    const ReactComponent = ReactNativeComponentTree.getInstanceFromNode(e.target)._currentElement
                    console.log(ReactComponent)
                    if (ReactComponent === 'Delete and create again' ) {
                        this.props.deleteSloganRequest(this.props.editSlogan.data.id)
                        this.props.showEditSloganDialog({...initialState, newDialog: true})
                    }else if( ReactComponent.props.children && ReactComponent.props.children === 'Delete and create again' ){
                        this.props.deleteSloganRequest(this.props.editSlogan.data.id)
                        this.props.showEditSloganDialog({ ...initialState, newDialog: true })
                    }
                }}>
                <View>
                    <Text >Slogan: {this.props.editSlogan.data.slogan} </Text>
                    <Text style={{marginTop: 15}}>Pseudo: {this.props.editSlogan.data.pseudo} </Text>
                    <Text style={{marginTop: 20}}>Prix:   {this.props.editSlogan.data.price} €</Text>
                </View>
            </MaterialDialog>
        )
    }
}
// eslint-disable-next-line max-len
export default connect(({editSlogan}) => ({editSlogan}),{deleteSloganRequest,showEditSloganDialog,pay})(SloganRequestDialog)