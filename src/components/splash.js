import React from 'react'
import {StyleSheet} from 'react-native'
import {
    View, Text,
} from 'native-base'
import Spinner from 'react-native-spinkit'
import {FONT_COLOR,BAKGRROUND_COLOR} from '../constants/color'


const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:BAKGRROUND_COLOR,
        justifyContent:'center',
        alignItems:'center'
    },
    text:{
        color:FONT_COLOR
    }
})

export default class Splash extends React.Component {
    render(){
        return(
            <View style={styles.container}>
                <Spinner size={70} type={'Wave'} color={FONT_COLOR} />
                <Text style={styles.text}>Veuillez patientez</Text>
            </View>
        )
    }
}