import React from 'react'
import PropTypes from 'prop-types'
import {StyleSheet, Animated} from 'react-native'
import {
    View, Text,
} from 'native-base'

import * as Animatable from 'react-native-animatable'

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#010101',
        minHeight: 300,
        minWidth: 400,
        
    },
})
export default class Slogan extends React.Component {


    render() {
        const sloganWithAnimation = <Animatable.View animation={'zoomIn'} duration={1500} style={styles.container} >
            <Text style={[{fontSize: 30, color: '#ffc107',fontFamily: 'Bookman Old Style'}, this.props.sloganStyles]}>{this.props.slogan}</Text>
            <Text style={[{fontSize: 20, fontWeight: 'bold', color: '#ffc107'}, this.props.pseudoStyles]}>{this.props.pseudo ? `- ${this.props.pseudo} -` : ''}</Text>
        </Animatable.View >

        const sloganWithoutAnimation = <View style={styles.container} >
            <Animated.Text style={[{ color: '#ffc107', fontFamily: 'Bookman Old Style'}, this.props.sloganStyles]}>{this.props.slogan}</Animated.Text>
            <Animated.Text style={[{fontWeight: 'bold', color: '#ffc107'}, this.props.pseudoStyles]}>{this.props.pseudo ? `- ${this.props.pseudo} -` : ''}</Animated.Text>
        </View >

        switch (this.props.selected) {
            case true: {
                return sloganWithAnimation
            }
            case false: {
                return sloganWithoutAnimation
            }
            default:
                return null
        }
    }
}

Slogan.propTypes = {
    slogan: PropTypes.string,
    pseudo: PropTypes.string,
    sloganStyles: PropTypes.object,
    pseudoStyles: PropTypes.object,


}
