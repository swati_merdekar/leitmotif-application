import React from 'react'
import { StyleSheet,KeyboardAvoidingView } from 'react-native'
import { Field, reduxForm } from 'redux-form'
import { View, Text } from 'native-base'
import { Actions } from 'react-native-router-flux'
import { MKTextField } from 'react-native-material-kit'
import _ from 'lodash'
import { appStore } from '../'

import {
    makeSloganRequest, updateSloganForm,
} from '../actions'

import { FONT_COLOR } from '../constants/color'

const styles = StyleSheet.create({
    textfieldWithFloatingLabel: {
        width: 300
    },

})

class CreateSloganForm extends React.Component {
    renderField = ({ input }) => {
        const errorColor = 'red'
        const fieldPropsError = () => {
            return this.props.sloganForm.errors && this.props.sloganForm.errors[input.name] ? { tintColor: errorColor, placeholderTextColor: errorColor, highlightColor: errorColor } : {}
        }
        return <View>
            <MKTextField
            textInputStyle={{color: FONT_COLOR, flex:1}}
                onTextChange={() => {
                    if (this.props.sloganForm.errors){
                        if (this.props.sloganForm.errors[input.name]) {

                            const errors = this.props.sloganForm.errors
                            delete errors[input.name]

                            appStore.dispatch(updateSloganForm(_.isEqual(errors, {}) ? {errors: null} : { errors }))
                            console.log('--------------------', this.props.sloganForm.errors, '---', input.name)
                        }
                    }
                }}
                tintColor={this.props.sloganForm.errors && this.props.sloganForm.errors[input.name] ? 'red' : FONT_COLOR}
                placeholderTextColor={this.props.sloganForm.errors && this.props.sloganForm.errors[input.name] ? 'red' : FONT_COLOR}
                placeholder={input.name}
                style={styles.textfieldWithFloatingLabel}
                floatingLabelEnabled={true}
                highlightColor={this.props.sloganForm.errors && this.props.sloganForm.errors[input.name] ? 'red' : FONT_COLOR}
                {...input}
                {...fieldPropsError() }
            />
        </View>
    }

    renderErrors(field) {
        if (this.props.sloganForm.errors && this.props.sloganForm.errors[field]) {
            return <Text note style={{ color: 'red' }}>{this.props.sloganForm.errors[field]}</Text>
        }
        return null
    }

    render() {
        return (
            <View >
            <Text style={{'color':'#ffc107','alignSelf':'center','fontWeight':'bold'}}>Create New Leitmotif</Text>
                <Field name="slogan" component={this.renderField} label="Slogan" />
                {this.renderErrors('slogan')}
                <Field name="pseudo" component={this.renderField} label="Pseudo" />
                {this.renderErrors('pseudo')}
            </View>
        )
    }
}

// eslint-disable-next-line no-class-assign
CreateSloganForm = reduxForm({
    form: 'createSloganForm',
    onSubmit: values => {
        console.log('************************** submit **************************')
        const { pseudo, slogan } = values
        let errors = {}
        if (pseudo && slogan) {
            errors = slogan.length > 35 ? { ...errors, slogan: 'max lenght of slogan must be 35' } : { ...errors }
            errors = pseudo.length > 35 ? { ...errors, slogan: 'max lenght of slogan must be 35' } : { ...errors }

            if (Object.keys(errors).length === 0) {
                appStore.dispatch(makeSloganRequest(values))
                //Actions.payment()
            }
            appStore.dispatch(updateSloganForm({ errors }))
        } else {
            errors = pseudo ? { ...errors } : { ...errors, pseudo: 'champ obligatoire' }
            errors = slogan ? { ...errors } : { ...errors, slogan: 'champ obligatoire' }
            appStore.dispatch(updateSloganForm({ errors }))

        }
    },
}, null, {})(CreateSloganForm)

export default CreateSloganForm
