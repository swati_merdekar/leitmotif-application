import React from 'react'
import { StatusBar,Modal,View,TouchableHighlight,Text,StyleSheet,KeyboardAvoidingView} from 'react-native'
import { connect } from 'react-redux'
import { MaterialDialog } from 'react-native-material-dialog'
import { Dialog } from 'react-native-simple-dialogs'
import {batchActions} from 'redux-batched-actions'

import { FONT_COLOR } from '../constants/color'
import { initialState } from '../reducers/edit-slogan'
import { showEditSloganDialog, updateSloganForm } from '../actions'
import Orientation from 'react-native-orientation'
import CreateSloganForm from './create-slogan-form'
import {appStore} from '../'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class CreateSloganDialog extends React.Component {
    componentDidMount() {
        Orientation.lockToLandscape()
        StatusBar.setHidden = false
    }
      state = {
    modalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
    render() {
        return (
     <View>
        <Modal 
          transparent={true}
          visible={this.props.editSlogan.newDialog}
          supportedOrientations={['landscape','landscape-left','landscape-right']}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
        <KeyboardAwareScrollView style={{flex:1,marginTop:'10%'}} behavior="padding">
            <View >
                <View style={{alignItems:'center',backgroundColor:'#6E6C6C','width':'50%','marginLeft':'25%','padding':'8%'}}>
                <CreateSloganForm sloganForm={this.props.sloganForm} ref={'createSloganForm'} />
                    <View style={{flexDirection: 'row'}}>
                        <TouchableHighlight style={styles.button}
                            onPress={() => {
                            this.refs.createSloganForm.submit()
                            //this.setModalVisible(!this.state.modalVisible);
                            }}>
                            <Text>ok</Text>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.button}
                            onPress={() => {
                            appStore.dispatch(batchActions([
                            showEditSloganDialog(initialState),
                            updateSloganForm({ errors: null }),
                            ]))
                            this.setModalVisible(!this.state.modalVisible);
                            }}>
                            <Text>Cancel</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
          </KeyboardAwareScrollView>
        </Modal>
      </View>
        );
    }
}
const styles = StyleSheet.create({
   container: {
      alignItems: 'center',
      justifyContent:'center',
      backgroundColor: '#ffffff',
      padding: 10,
      margin:0
   },
  button: {
    backgroundColor: '#ffc107',
    padding: 10,
    alignSelf: 'flex-end', 
    margin:10
  },
     modal: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#f7021a',
      padding: 100
   }
})
// eslint-disable-next-line max-len
export default connect(({ editSlogan, sloganForm }) => ({ editSlogan, sloganForm }), { showEditSloganDialog, updateSloganForm })(CreateSloganDialog)
