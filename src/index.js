import React from 'react'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import promiseMiddleware from 'redux-promise'
import thunkMiddleware from 'redux-thunk'
import {Router, Scene, Actions} from 'react-native-router-flux'
import {AsyncStorage, StatusBar, View} from 'react-native'
import {autoRehydrate, persistStore} from 'redux-persist'
import {enableBatching} from 'redux-batched-actions'

import reducers from './reducers'
import User from './modules/user/components'
import Home from './modules/home/components'
import Register from './modules/register/components'
import Login from './modules/login/components'
import SlogansStars from './modules/slogans-stars/components'
import Splash from './components/splash'
import ForgotPassword from './modules/forgot-password/components'
import Payment from './modules/payment/components'




import {loadUser, updatePersist, updateSession} from './actions'
//import {refreshToken} from './services/session'

import { TOKEN_STORAGE_KEY } from './constants'


export const appStore = createStore(
    enableBatching(reducers),
    autoRehydrate(),
    applyMiddleware(promiseMiddleware,thunkMiddleware),
)

// export const persist = persistStore(appStore,{
//     storage:AsyncStorage,
//     blacklist:[],
// },appStore.dispatch(updatePersist({isHydrated:true})))
console.disableYellowBox = true;

export default class App extends React.Component {

    
    componentDidMount() {
        // const unsubscribe = appStore.subscribe(() => {
        //     console.log('-> unsubscribe')
        //     if (appStore.getState().persist.isHydrated) {
        //         console.log('-> isHydrated true')
        //         unsubscribe()
        //     }
        // })



        AsyncStorage.getItem(TOKEN_STORAGE_KEY)
            .then(userAuthenticatedData => {
                if (userAuthenticatedData === null) {
                    const userAuthenticated = {
                        token: {
                            value: null,
                        },
                        user: {
                            id: null,
                        },
                    }
                    appStore.dispatch(updateSession(userAuthenticated))
                    Actions.login()
                } else {
                    appStore.dispatch(updateSession(JSON.parse(userAuthenticatedData)))
                    Actions.home()
                }
            })
            .catch(error => {
                Actions.login()
                console.log(error)
            })

    }
    // autoLogin() {
    //     refreshToken().then(() => {
    //         console.log('refresh success')
    //     }).catch(() => {
    //         console.log('refresh faild')
    //     })
    // }



    render() {
        const headerStyle = {
            backgroundColor: '#3f51b5',
            color: '#FFF',
        }
        return (
            <View style={{flex: 1}}>
                <StatusBar hidden />
                <Provider store={appStore}>
                    <Router>
                        <Scene key="root">
                            <Scene
                                key="splash"
                                component={Splash}
                                hideNavBar={true}
                                initial
                            />
                            <Scene
                                key="home"
                                component={Home}
                                hideNavBar={true}
                            />
                            <Scene
                                key="slogansStars"
                                component={SlogansStars}
                                hideNavBar={true}
                            />
                            <Scene
                                navigationBarStyle={{backgroundColor: headerStyle.backgroundColor}}
                                titleStyle={{color: headerStyle.color}}
                                key="user"
                                component={User}
                                hideNavBar={false}
                                title="User"
                            />
                            <Scene
                                key="register"
                                component={Register}
                                hideNavBar={true}
                            />
                            <Scene
                                key="login"
                                component={Login}
                                hideNavBar={true}
                            />
                            <Scene
                                key="forgotPassword"
                                component={ForgotPassword}
                                hideNavBar={true}
                            />
                            <Scene
                                key="payment"
                                component={Payment}
                                hideNavBar={true}
                            />
                        </Scene>
                    </Router>
                </Provider>
            </View>

        )
    }
}
