import {LOGIN__ERRORS} from '../constants'

export default function(state = {}, action) {
    switch (action.type) {
        case LOGIN__ERRORS: {
            return action.payload
        }
        default:
            return state
    }
}
