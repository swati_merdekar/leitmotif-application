import {CREATE_SLOGAN_PROCESSING} from '../constants'

export default function(state = false, action) {
    switch (action.type) {
        case CREATE_SLOGAN_PROCESSING: {
            return action.payload
        }
        default:
            return state
    }
}
