import {combineReducers} from 'redux'
import {reducer as form} from 'redux-form'
import user from './user'
import selectedSlogan from './selected-slogan'
import felicitationDialogProcess from './felicitation-dialog-process'
import registerProcessing from './register-process'
import loginProcessing from './login-process'
import loginErrors from './login-errors'
import registerError from './register-errors'
import drawerIsOpen from './drawer-is-open'
import editSlogan from './edit-slogan'
import session from './session'
import persist from './persist'
import accessToken from './access-token'
import slogans from './slogans'
import selectedSloganStar from './selected-slogan-star'
import sloganForm from './slogan-form'
import forgotPassword from './forgot-password'
// eslint-disable-next-line import/max-dependencies
import createSloganProcessing from './create-slogan-process'




export default combineReducers({
    user,
    selectedSlogan,
    form,
    felicitationDialogProcess,
    registerProcessing,
    loginProcessing,
    loginErrors,
    registerError,
    drawerIsOpen,
    editSlogan,
    session,
    persist,
    accessToken,
    slogans,
    selectedSloganStar,
    sloganForm,
    forgotPassword,
    createSloganProcessing,
})
