

export const initialState = {
    slogans: [],
    slogansStars: [],
}
export default function(state = initialState, action) {
    switch (action.type) {
        case 'UPDATE_SLOGANS': {
            return action.payload
        }
        default:
            return state
    }
}
