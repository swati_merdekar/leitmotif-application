import {UPDATE_PERSIST} from '../constants'

export const initialState={
    isHydrated:false,
}

export default function (state=initialState,action) {
    switch (action.type) {
        case UPDATE_PERSIST:{
            return action.payload
        }
        default:
            return state
    }
}