import {REGISTER_ERRORS} from '../constants'

export default function(state = null, action) {
    switch (action.type) {
        case REGISTER_ERRORS: {
            return action.payload
        }
        default:
            return state
    }
}
