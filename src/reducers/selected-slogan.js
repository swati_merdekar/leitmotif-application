import {SELECTED_SLOGAN} from '../constants'


export default function(state = {}, action) {
    switch (action.type) {
        case SELECTED_SLOGAN: {
            return action.payload
        }
        default:
            return state
    }
}
