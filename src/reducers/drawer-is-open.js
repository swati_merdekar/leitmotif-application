import {OPEN_DRAWER} from '../constants'

export default function(state = false, action) {
    switch (action.type) {
        case OPEN_DRAWER: {
            return action.payload
        }
        default:
            return state
    }
}
