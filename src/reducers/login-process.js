import { LOGIN_PROCESS } from '../constants'

export default function(state = false, action) {
    switch (action.type) {
        case LOGIN_PROCESS: {
            return action.payload
        }
        default:
            return state
    }
}
