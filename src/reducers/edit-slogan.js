

export const initialState = {
    isExist: false,
    requestDialog: false,
    newDialog: false,
    editDialog: false,
    data: {}


}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'EDIT_SLOGAN_DIALOG': {
            return { ...state, ...action.payload }
        }
        default:
            return state
    }
}
