import { REGISTER_PROCESS } from '../constants'



export default function(state = false, action) {
    switch (action.type) {
        case REGISTER_PROCESS: {
            return action.payload
        }
        default:
            return state
    }
}
