import {SELECTED_SLOGAN_STAR} from '../constants'


export default function(state = {}, action) {
    switch (action.type) {
        case SELECTED_SLOGAN_STAR: {
            return action.payload
        }
        default:
            return state
    }
}
