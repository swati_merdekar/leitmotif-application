import {UPDATE_SLOGAN_FORM} from '../constants'

export const initialState = {
    errors: null,
}

export default function(state = initialState, action) {
    switch (action.type) {
        case UPDATE_SLOGAN_FORM: {
            return action.payload
        }
        default:
            return state
    }
}