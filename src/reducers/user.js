import {LOAD_USER} from '../constants'

export default function(state = null, action) {
    switch (action.type) {
        case LOAD_USER: {
            if (action.payload.data) {
                return {email: action.payload.data.email}
            }
            return null
        }
        default:
            return state
    }
}
