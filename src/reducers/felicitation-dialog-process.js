import {TOGGLE_FELICITATION_DIALOG} from '../constants'

export default function(state = false, action) {
    switch (action.type) {
        case TOGGLE_FELICITATION_DIALOG: {
            return !state
        }
        default:
            return state
    }
}
