

export default function(state = false, action) {
    switch (action.type) {
        case 'NEW_SLOGAN_DIALOG': {
            return action.payload
        }
        default:
            return state
    }
}
