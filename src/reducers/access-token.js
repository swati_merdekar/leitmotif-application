import {TOKEN_STORAGE_KEY} from '../constants'

export const initialState = {
    value: null,
}

export default function(state = initialState, action) {
    switch (action.type) {
        case TOKEN_STORAGE_KEY: {
            return action.payload
        }
        default:
            return state
    }
}
