import { UPDATE_SESSION } from '../constants'

export const initialState = {
    token: {
        value: null,
    },
    user: {
        id: null,
    }
}

export default function (state = initialState, action) {
    switch (action.type) {
        case UPDATE_SESSION: {
            return { ...action.session }
        }
        default:
            return state
    }
}

