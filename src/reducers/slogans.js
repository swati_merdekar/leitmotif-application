import {Alert} from 'react-native'

export const initialState = {
    slogans: [],
    slogansStars: [],
}
export default function(state = initialState, action) {
    switch (action.type) {
        case 'LOAD_SLOGANS_STARTS': {
            if (action.payload.data) {
                return {...state, slogansStars: action.payload.data}
            } else {
                Alert.alert('network erros', 'check your connection please.')
                return state
            }

        }
        case 'ADD_SLOGAN': {
            return {...state, slogans: [action.payload, ...state.slogans]}
        }
        case 'LOAD_SLOGANS': {
            if (action.payload.data) {
                return {...state, slogans: action.payload.data}
            } else {
                Alert.alert('network erros', 'check your connection please.')
                return state
            }
        }
        default:
            return state
    }
}
