import { FORGOT_PASSWORD_PROCESSING, FORGOT_PASSWORD_ERRORS, FORGOT_PASSWORD_MESSAGE_SUCCESS } from '../constants'


export const initialState = {
    errors: null,
    isProcessing: false,
    success: null,
}

export default function(state = initialState, action) {
    switch (action.type) {
        case FORGOT_PASSWORD_PROCESSING: {
            return { ...state, isProcessing: action.payload }
        }
        case FORGOT_PASSWORD_ERRORS: {
            return { ...state, success: null, errors: action.payload }
        }
        case FORGOT_PASSWORD_MESSAGE_SUCCESS: {
            return { ...state, errors: null, success: action.payload }
        }
        default: {
            return state
        }
    }
}
