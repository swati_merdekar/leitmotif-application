import { Buffer } from 'buffer'
import { fetchApi } from '../api'



const endPoints = {
    authenticate: '/users/auth',
    revoke: '/users/auth/revoke',
    refresh: '/users/auth/refresh',
    login: '/login',
    register: '/register',
}

export const authenticate = () => fetchApi()

export const login = (email, password) => fetchApi(endPoints.login, { email, password }, 'post', {
    Authorization: null,
})

export const register = (
    username, email, password, confirmPssword, accept = true, role) => fetchApi(endPoints.register,
    { username, password, confirmPssword, accept, role }, 'post', {
        Authorization: null,
    })

export const refresh = (token, user) => fetchApi(endPoints.refresh, { token, user }, 'post', {
    'Client-ID': 'oooooooo',
    Authorization: null,
})

export const revoke = token => fetchApi()