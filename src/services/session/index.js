import {appStore} from '../../'
import {updateSession} from '../../actions'
import {initialState} from '../../reducers/session'

let sessionTimeout=null

const clearSession=()=>{
    clearTimeout(sessionTimeout)
    appStore.dispatch(updateSession(initialState))
}

export const refreshToken = ()=>{
    const session= appStore.getState().session
    console.log('-------refreshToken function -----------')
    if(!session.tokens.refresh.value || session.user.id) {
        return Promise.reject()
    }
}