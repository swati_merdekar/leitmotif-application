import axios from 'axios'
import _ from 'lodash'
import {appStore} from '../../'
import {API_ENDPOINT,BASE_URL} from '../../constants/api'

export const fetchApi=(endPoint,payload={},method='get',headers={})=>{
    const accessToken=appStore.getState().session.tokens.access.value
    return axios({
        method:method.toLowerCase(),
        url:`${BASE_URL}${endPoint}`,
        headers:_.pickBy({
            ...(accessToken?{
                Authorization: `Bearer ${accessToken}`,
            }:{}),
            ...headers,
        }, item => !_.isEmpty(item))
    }).catch(error=>{
        console.log('service/api/index:fetchApi ',error)
    })
}