import { AsyncStorage } from 'react-native'
import axios from 'axios'
import fetchival from 'fetchival'
import { TOKEN_STORAGE_KEY } from './../constants'
import { API_ENDPOINT, BASE_URL } from './../constants/api'



// export const fetchApi = () => {

//     AsyncStorage.getItem(TOKEN_STORAGE_KEY)
//         .then(tokenKey => {
//             console.log('------------------toekn ------------------', tokenKey)
//             if (tokenKey !== null) {
//                 axios({
//                     method: 'GET',
//                     url: 'http://admin.leitmotif:5000/api/v2/last_requests',
//                     headers: {
//                         Accept: 'application/json',
//                         'Content-Type': 'application/json',
//                         Authorization: '.eJwVxrENgDAMBMBdXBOJxMbvzIIoEvPefwTEVXcLVW2_zhao2eyMf4k2PLKIrqZdDolpdLjZUiWuPYuVAH2YZjnk-QDUnBNO.DJA0BQ.iHoPEgiuoW7p9M9DUBswVriUGZo',

//                     },
//                 }).then(s => console.log(s)).catch(m => console.log(m))
//             }
//         })
//         .catch(error => {
//             console.log(error)
//         })
// }



export default class Auth {
    static get(request) {
        AsyncStorage.getItem(TOKEN_STORAGE_KEY)
            .then(tokenKey => {
                console.log('------------------toekn ------------------', tokenKey)
                if (tokenKey !== null) {
                    return axios({
                        method: 'GET',
                        url: `${API_ENDPOINT}${request}`,
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: '.eJwVxrENgDAMBMBdXBOJxMbvzIIoEvPefwTEVXcLVW2_zhao2eyMf4k2PLKIrqZdDolpdLjZUiWuPYuVAH2YZjnk-QDUnBNO.DJA0BQ.iHoPEgiuoW7p9M9DUBswVriUGZo',

                        },

                    })
                }
            })
            .catch(error => {
                console.log(error)
            })


    }



    static post(request, data) {
        AsyncStorage.getItem(TOKEN_STORAGE_KEY)
            .then(tokenKey => {
                if (tokenKey !== null) {
                    return axios.post({
                        method: 'POST',
                        url: `${API_ENDPOINT}${request}`,
                        'Content-Type': 'application/json',
                        Authorization: tokenKey,
                    }, data)
                }

            })
            .catch(error => {
                console.log(error)
            })
    }

}


